-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 10:32 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `HBR090721`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/07/08 11:45:02', '2021-07-08', '2021-07-08', 1, 'Abbott'),
(2, 'SUJESH V S', 'sujesh.vs@abbott.com', 'Cochin', 'Abbott', NULL, NULL, '2021/07/09 18:01:28', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(3, 'SUJESH V S', 'sujesh.vs@abbott.com', 'Cochin', 'Abbott', NULL, NULL, '2021/07/09 18:01:28', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(4, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/07/09 18:51:58', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(5, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/07/09 19:06:42', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(6, 'SUJESH V S', 'sujesh.vs@abbott.com', 'Cochin', 'Abbott', NULL, NULL, '2021/07/09 19:20:53', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(7, 'Dr Anand Kumar V', 'anandheart@gmail.com', 'Kochi', 'VPS Lakeshore Global Lifecare', NULL, NULL, '2021/07/09 19:38:26', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(8, 'Sudheesh Chandran', 'sudheesh.chandran@abbott.com', 'Trivandrum', 'Abbott', NULL, NULL, '2021/07/09 19:42:34', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(9, 'Dr Anand Kumar V', 'anandheart@gmail.com', 'Kochi', 'VPS Lakeshore Global Lifecare', NULL, NULL, '2021/07/09 19:44:13', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(10, 'Sajan Narayanan', 'sajannarayanan@gmail.com', 'Kochi', 'Little Flower Hospital & Research Centre', NULL, NULL, '2021/07/09 19:52:13', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(11, 'dr mahesh', 'drnkmahesh@gmail.com', 'parumala', 'PICC', NULL, NULL, '2021/07/09 19:52:17', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(12, 'Ajith Jacob', 'ajith.jacob@abbott.com', 'Cochin', 'AV', NULL, NULL, '2021/07/09 19:55:35', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(13, 'Raghuram A Krishnan', 'raghu_rak@hotmail.com', 'Kozhikode', 'BABY MEMORIAL HOSPITAL', NULL, NULL, '2021/07/09 19:56:31', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(14, 'Jabir Abdullakutty', 'drjabi@yahoo.co.in', 'Kochi', 'lisie', NULL, NULL, '2021/07/09 20:02:36', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(15, 'SAJAN NARAYANAN', 'sajannarayanan@outlook.com', 'Kochi', 'Little Flower Hospital & Research Centre', NULL, NULL, '2021/07/09 20:15:57', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(16, 'Sajan Narayanan', 'sajannarayanan@gmail.com', 'Kochi', 'Little flower Hospital & research Institute', NULL, NULL, '2021/07/09 20:17:04', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(17, 'SAJAN NARAYANAN', 'sajannarayanan@gmail.com', 'Kochi', 'Little Flower Hospital & Research Centre', NULL, NULL, '2021/07/09 20:17:39', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(18, 'Dr Anand', 'anandheart@gmail.com', 'Kochi', 'Lakeshore', NULL, NULL, '2021/07/09 20:20:24', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(19, 'SAJAN NARAYANAN', 'sajannarayanan@gmail.com', 'Kochi', 'Little Flower Hospital & Research Centre', NULL, NULL, '2021/07/09 20:28:24', '2021-07-09', '2021-07-09', 1, 'Abbott'),
(20, 'jimmy george', 'pjimmygeorge@gmail.com', 'Ernakulam', 'Lisie ', NULL, NULL, '2021/07/09 20:49:19', '2021-07-09', '2021-07-09', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'neeraj', 'pandurevanthreddy002@gmail.com', 'NELLORE', '9700467764', NULL, NULL, '2021-07-08 11:42:46', '2021-07-08 11:42:46', '2021-07-08 13:12:46', 0, 'Abbott', 'aaf14e1d7cff78b4826224bbc60a6ddb'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-07-08 11:44:27', '2021-07-08 11:44:27', '2021-07-08 11:44:49', 0, 'Abbott', 'f4efd5b788680d9aa2f5d6acb56d5b8d'),
(3, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-07-08 11:47:53', '2021-07-08 11:47:53', '2021-07-08 11:47:58', 0, 'Abbott', '957fbf1ac49974a4f0f1faaefc3c64b3'),
(4, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-07-09 10:08:43', '2021-07-09 10:08:43', '2021-07-09 10:08:55', 0, 'Abbott', 'dc02be00184cbf3d754ca4270ea4375b'),
(5, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-07-09 18:23:01', '2021-07-09 18:23:01', '2021-07-09 18:23:05', 0, 'Abbott', '15128b8ec4eebee3e6879bac05cdfb79'),
(6, 'Santosh Chandran', 'santhosh.chandran@abbott.com', 'Kochi', 'Abbott ', NULL, NULL, '2021-07-09 19:07:48', '2021-07-09 19:07:48', '2021-07-09 20:37:48', 0, 'Abbott', 'e34e91cf05d709c8a2db9944bead1486'),
(7, 'Athmika Manjesh', 'athmika_m.in@jtbap.com', 'Bangalore', 'JTB India Private Limited', NULL, NULL, '2021-07-09 19:16:01', '2021-07-09 19:16:01', '2021-07-09 20:46:01', 0, 'Abbott', '18bdce993794a307b311e7356ad6ae7f'),
(8, 'dr mahesh', 'drnkmahesh@gmail.com', 'parumala', 'st gregorious hospital', NULL, NULL, '2021-07-09 19:50:53', '2021-07-09 19:50:53', '2021-07-09 21:20:53', 0, 'Abbott', 'f63cf3e0fd389960ff7d83a5285a0187'),
(9, 'Sojo Joy', 'sojo.joy@abbott.com', 'Cochin', 'Abbott', NULL, NULL, '2021-07-09 20:01:36', '2021-07-09 20:01:36', '2021-07-09 21:31:36', 0, 'Abbott', 'b787d19240e6457b0ecfba877b493d0e'),
(10, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-07-09 20:03:42', '2021-07-09 20:03:42', '2021-07-09 20:03:49', 0, 'Abbott', '24e5bf9e256ff2264cfce8cca033416b'),
(11, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-07-09 20:04:56', '2021-07-09 20:04:56', '2021-07-09 20:05:01', 0, 'Abbott', '68145368c9df0771e46855b1d05b831e'),
(12, 'Sudheesh Chandran', 'sudheesh.chandran@abbott.com', 'Trivandrum', 'Abbott', NULL, NULL, '2021-07-09 20:05:18', '2021-07-09 20:05:18', '2021-07-09 21:35:18', 0, 'Abbott', '87db9b5fb06ee8b3adf72f4ab39403d8'),
(13, 'SARATH S K', 'sarathsksj@gmail.com', 'Thiruvananthapuram', 'Abbott', NULL, NULL, '2021-07-09 20:05:22', '2021-07-09 20:05:22', '2021-07-09 21:35:22', 0, 'Abbott', '904cc04fe22ca3afed436b283770245a'),
(14, 'Santosh Chandran', 'santhosh.chandran@abbott.com', 'Kochi', 'Abbott ', NULL, NULL, '2021-07-09 20:05:32', '2021-07-09 20:05:32', '2021-07-09 21:35:32', 0, 'Abbott', 'b52ce38dab7e6a8fcded7b4c6e4a9690'),
(15, 'KC Rathan ', 'Kamanatchanolian.rathan@abbott.com', 'Chennai ', 'Abbott ', NULL, NULL, '2021-07-09 20:05:43', '2021-07-09 20:05:43', '2021-07-09 21:35:43', 0, 'Abbott', '87a0e82993a17397c2b98f445b10008b'),
(16, 'ramesh nair', 'ramesh.mnair@abbott.com', 'Calicut ', 'NA', NULL, NULL, '2021-07-09 20:05:48', '2021-07-09 20:05:48', '2021-07-09 21:35:48', 0, 'Abbott', 'f42ead997c84f6787b9beb9743986078'),
(17, 'PARVATHY', 'parvatypv1989@gamil.com', 'TRIVANDRUM ', 'SUT', NULL, NULL, '2021-07-09 20:07:37', '2021-07-09 20:07:37', '2021-07-09 21:37:37', 0, 'Abbott', '04fb7d0004bd799e55b7c49050a14b69'),
(18, 'Athmika Manjesh', 'athmika_m.in@jtbap.com', 'Bangalore', 'JTB India Private Limited', NULL, NULL, '2021-07-09 20:08:16', '2021-07-09 20:08:16', '2021-07-09 21:38:16', 0, 'Abbott', '2bc81b6104f15d549e93f38bb32c3915'),
(19, 'Athmika Manjesh', 'athmika_m.in@jtbap.com', 'Bangalore', 'JTB India Private Limited', NULL, NULL, '2021-07-09 20:08:16', '2021-07-09 20:08:16', '2021-07-09 21:38:16', 0, 'Abbott', '45bc6f3b3dc8b944193bd3de170de989'),
(20, 'Jibin Thariyan Thomas', 'jibintthomas@gmail.com', 'Kochi', 'Lisie ', NULL, NULL, '2021-07-09 20:08:59', '2021-07-09 20:08:59', '2021-07-09 21:38:59', 0, 'Abbott', '1f85c6bafd1cab83a3c5915d5b7f0892'),
(21, 'Abilash', 'abilash1986@gmail.com', 'Trivandrum', 'GG', NULL, NULL, '2021-07-09 20:09:05', '2021-07-09 20:09:05', '2021-07-09 21:39:05', 0, 'Abbott', '94c5451782eab5533597cfea2afcc686'),
(22, 'Swathy', 'swarenju96@gmail.com', 'Kollam', 'Ns mims', NULL, NULL, '2021-07-09 20:09:10', '2021-07-09 20:09:10', '2021-07-09 21:39:10', 0, 'Abbott', '87b673019e5378ae316b74f047da6655'),
(23, 'Sreethu', 'sreeinhimmel@gmail.com', 'Trivandrum', 'Jubilee Hospital', NULL, NULL, '2021-07-09 20:09:32', '2021-07-09 20:09:32', '2021-07-09 20:32:37', 0, 'Abbott', 'fffd75da4286e69aef73d45260adc3bf'),
(24, 'Sakkeer', 'shaheer310@gmail.com', 'Alappuzha', 'V S M ', NULL, NULL, '2021-07-09 20:12:01', '2021-07-09 20:12:01', '2021-07-09 21:42:01', 0, 'Abbott', 'cb7533b0e3b4043a62db86e614f8cf48'),
(25, 'Toji', 'thomastoji1992@gamil.com', 'Thrissur', 'Daya hospital ', NULL, NULL, '2021-07-09 20:12:49', '2021-07-09 20:12:49', '2021-07-09 20:15:14', 0, 'Abbott', 'b9d85845a31e8e0c4ca3c993bc85396c'),
(26, 'Sethu', 'sethuparu83@gmail.com', 'Tvm', 'Sree chitra', NULL, NULL, '2021-07-09 20:12:51', '2021-07-09 20:12:51', '2021-07-09 20:14:12', 0, 'Abbott', 'f7c8562a0a023aa61a0aafc8b28d1a15'),
(27, 'Rohit a', 'a.rohit@abbott.com', 'Coimbatore ', 'Av', NULL, NULL, '2021-07-09 20:12:52', '2021-07-09 20:12:52', '2021-07-09 21:42:52', 0, 'Abbott', 'd349f27e3d201d56703b9639d24c4f4f'),
(28, 'Simboola', 'simboolafathima@gmail.com', 'Trivandrum', 'Mch tvm', NULL, NULL, '2021-07-09 20:13:38', '2021-07-09 20:13:38', '2021-07-09 21:43:38', 0, 'Abbott', 'f90f72b4cedb85e457de591f51194538'),
(29, 'Ajith Jacob', 'ajith.jacob@abbott.com', 'Cochin', 'AV', NULL, NULL, '2021-07-09 20:18:17', '2021-07-09 20:18:17', '2021-07-09 21:48:17', 0, 'Abbott', '036c476034b5647448dca064e6382bd0'),
(30, 'Navajith V T', 'navajith.thankam@abbott.com', 'Calicut', 'AV', NULL, NULL, '2021-07-09 20:19:29', '2021-07-09 20:19:29', '2021-07-09 21:49:29', 0, 'Abbott', '3200589a4c458bcefd51b70a87db53d2'),
(31, 'Toji', 'thomastoji1992@gamil.com', 'Thrissur', 'Daya hospital', NULL, NULL, '2021-07-09 20:19:59', '2021-07-09 20:19:59', '2021-07-09 21:49:59', 0, 'Abbott', '07e955c5c9da17cb649698cddb41d87e'),
(32, 'Jibin Thariyan Thomas', 'jibintthomas@gmail.com', 'Kochi', 'Lisie hospital ', NULL, NULL, '2021-07-09 20:21:11', '2021-07-09 20:21:11', '2021-07-09 21:51:11', 0, 'Abbott', 'a8f04d4390c589732dd31282b44ed368'),
(33, 'RADHIKA DEVI', 'devi.manu13@gmail.com', 'Thiruvananthapuram', 'PRS Hospital', NULL, NULL, '2021-07-09 20:23:15', '2021-07-09 20:23:15', '2021-07-09 21:53:15', 0, 'Abbott', 'b289552ea6884cedb37a300d6f14ca11'),
(34, 'RADHIKA DEVI', 'devi.manu13@gmail.com', 'Thiruvananthapuram', 'PRS Hospital', NULL, NULL, '2021-07-09 20:24:32', '2021-07-09 20:24:32', '2021-07-09 21:54:32', 0, 'Abbott', '052bb6a5ce165168a992aa93363e396f'),
(35, 'Sakkeer', 'shaheer310@gmail.com', 'Alappuzha', 'V s m', NULL, NULL, '2021-07-09 20:30:42', '2021-07-09 20:30:42', '2021-07-09 22:00:42', 0, 'Abbott', 'e30f330bfff8947ab5ce9f2dd4e73d69'),
(36, 'Dr.Prashanth', 'prasanthgv@gmail.com', 'Kozhikkode', 'Kmct', NULL, NULL, '2021-07-09 20:34:51', '2021-07-09 20:34:51', '2021-07-09 22:04:51', 0, 'Abbott', '6967fa744bdbadc9225e438370d1c35a'),
(37, 'Dr Sandeep ', 'sand@gmail.com', 'Calicut ', 'KMCT ', NULL, NULL, '2021-07-09 20:35:52', '2021-07-09 20:35:52', '2021-07-09 22:05:52', 0, 'Abbott', 'ddadb2d9ada2e0c201d732fedd9266f6'),
(38, 'Lijo ', 'lijo.k@abbott.com', 'Cochin ', 'Abbott ', NULL, NULL, '2021-07-09 20:37:33', '2021-07-09 20:37:33', '2021-07-09 22:07:33', 0, 'Abbott', 'a6aea92f8879a704bc97ddaa92eb4ece'),
(39, 'Ajith Jacob', 'ajith.jacob@abbott.com', 'Cochin', 'AV', NULL, NULL, '2021-07-09 20:39:00', '2021-07-09 20:39:00', '2021-07-09 22:09:00', 0, 'Abbott', '3f0fa49977ee94affb0ff5ff8b6a0373'),
(40, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-07-09 20:54:22', '2021-07-09 20:54:22', '2021-07-09 22:24:22', 0, 'Abbott', 'c0b7d2ccb927c4d1088d06e966c81e34'),
(41, 'Ajesh T', 'ajesh.t@abbott.com', 'Calicut', 'Abbott', NULL, NULL, '2021-07-09 21:17:56', '2021-07-09 21:17:56', '2021-07-09 22:47:56', 0, 'Abbott', 'c15984822ca9d7629bc51b6d7c2ae066'),
(42, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-07-09 21:29:49', '2021-07-09 21:29:49', '2021-07-09 22:59:49', 0, 'Abbott', '69d85a83838719e0a3227b4296e19fdc');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
